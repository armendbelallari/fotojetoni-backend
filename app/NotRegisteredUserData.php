<?php
/**
 * Created by PhpStorm.
 * User: valto
 * Date: 5/22/2018
 * Time: 7:43 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class NotRegisteredUserData extends Model
{

    protected $table = 'not_registered_user_data';

    public $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'other_name',
        'other_lastName',
        'other_phone',
        'other_city',
        'other_email',
        'other_country',
        'other_address',
        'other_street',
        'other_is_done',
        'other_done_time',
        'other_street',
        'platform',
        'other_terms_condition',
        'other_image_files_url',
        'other_image_number'

    ];


}