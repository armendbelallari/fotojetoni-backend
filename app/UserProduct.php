<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProduct extends Model
{
    protected $table = 'user_product';

    public $primaryKey = 'id';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'image_files_url',
        'image_number',
        'is_done',
        'platform',
        'done_time'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}