<?php
/**
 * Created by PhpStorm.
 * User: valtonpireci
 * Date: 8/26/18
 * Time: 18:34
 */

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use function foo\func;
use http\Env\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\UserProduct;
use App\User;
use App\NotRegisteredUserData;
use Carbon\Carbon;
use Validator;

class AdminPanelController extends Controller
{

    public $successStatus = 200;

    public function new_client_request()
    {
        $notDoneClients = UserProduct::with('user')->where('is_done', 0)->get();
        return response($notDoneClients);
    }

    public function getFile($fileName)
    {

        $this->fileName = '';
        $filePath = 'app/public/uploads/' . $fileName;

        /*DB::table('user_product')
            ->where('image_files_url', $fileName)
            ->update(['is_done' => 1, 'done_time' => now()]);*/

        $headers = [
            'Content-Description' => 'File Transfer',
            'Content-Type' => 'application/json',
        ];

        return response()->download(storage_path($filePath), $fileName, $headers);
    }

    public function new_notregistered_client_request()
    {

        $notDoneData = NotRegisteredUserData::where('other_is_done', null)->get();

        return response()->json($notDoneData, $this->successStatus);
    }

    public function done_notregistered_client(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'done_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $id = $request->input('done_id');

        DB::table('not_registered_user_data')
            ->where('id', $id)
            ->update(['other_is_done' => 1, 'other_done_time' => now()]);

        return response()->json(['success' => true], $this->successStatus);

    }
}