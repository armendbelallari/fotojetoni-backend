<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Jobs\SendVerificationEmail;
use Illuminate\Auth\Events\Registered;


class UserController extends Controller
{
    public $successStatus = 200;
    public $validatorError = 401;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $verified = $user->verified;
            if ($verified === 1) {
                $success['id'] = $user->id;
                $success['email'] = $user->email;
                $success['name'] = $user->name;
                $success['lastName'] = $user->lastName;
                $success['phone'] = $user->phone;
                $success['city'] = $user->city;
                $success['address'] = $user->address;

                $success['token'] = $user->createToken('MyApp')->accessToken;
                return response()->json($user);

            } else {
                $error['statusCode'] = $this->validatorError;
                $error['errorTitle'] = 'BonFoto';
                $error['errorMessage'] = 'Email-i nuk ëshë konfirmuar!';
                return response()->json(['error' => $error], 401);
            }


        } else {
            $error['statusCode'] = $this->validatorError;
            $error['errorTitle'] = 'BonFoto';
            $error['errorMessage'] = 'Email-i dhe/ose fjalëkalimi janë gabim!';
            return response()->json(['error' => $error], 401);
        }
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $userExist = User::where('email', '=', request('email'))->first();
        if ($userExist === null) {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required',
                'name' => 'required',
                'lastName' => 'required',
                'phone' => 'required',
                'city' => 'required',
                'country' => 'nullable',
                'address' => 'required',
                'street' => 'nullable',
                'termsCondition' => 'required'

            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }

            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $input['verified'] = 1;

            $input['email_token'] = base64_encode($input['email']);

            $user = User::create($input, ['verified' => 1]);

            $success['email'] = $user->email;
            $success['name'] = $user->name;
            $success['lastName'] = $user->lastName;
            $success['phone'] = $user->phone;
            $success['city'] = $user->city;
            $success['address'] = $user->address;
            $success['termsCondition'] = $user->termsCondition;
            $success['token'] = $user->createToken('MyApp')->accessToken;

            dispatch(new SendVerificationEmail($user));
            return response()->json(['success' => $success], $this->successStatus);

        } else {
            $error['statusCode'] = $this->validatorError;
            $error['errorTitle'] = 'BonFoto';
            $error['errorMessage'] = 'Email-i ekziston.';

            return response()->json(['error' => $error], 401);
        }

    }

    /**
     * Handle a registration request for the application.
     *
     * @param $token
     * @return \Illuminate\Http\Response
     */
    public function activate($token)
    {
        $user = User::where('email_token', $token)->first();
        $user->verified = 1;

        if ($user->save()) {
            return view('emailconfirm', ['user' => $user]);
        }
    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
}