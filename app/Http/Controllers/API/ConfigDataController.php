<?php
/**
 * Created by PhpStorm.
 * User: valtonpireci
 * Date: 7/7/18
 * Time: 19:26
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;

class ConfigDataController extends Controller
{

    public function config() {


        $config = array(
            'app_name' => 'BonFoto',
            'price_per_photo' => '0.15',
            'currency' => 'EUR',
            'delivery_costs' => '2.00',
            'min_photo_selection' => '1',
            'max_photo_selection' => '150',
            'app_language' => 'sq',
            'image_compression_level' => '0.9', // This value should be double value (1.0, 0.8, 0.75, 0.5, 0.25)
            'image_scale_size' => '1400',
            'image_scale_size_for_a4' => '2800',
            'show_a4_format' => '1', // visibility == 0 false / 1 true
            'a4_selection_limit' => '1',
            'a4_format_title' => 'Zgjedh Foto A5',
            'photo_formats' => [
                [
                    'format_id' => 1,
                    'name' => 'Format 10x15',
                    'format' => '10x15',
                    'price' => '0.15'
                ],[
                    'format_id' => 2,
                    'name' => 'Format 13x18',
                    'format' => '13x18',
                    'price' => '0.20'
                ]
                ],
            'albums' => [
                [
                    'album_id' => 'BF1015001',
                    'format' => '10x15',
                    'name' => 'A1-100',
                    'price' =>'3.00',
                    'capacity' => '100',
                    'photo_link' => 'http://fotojetoni.com/albums/A1-100.jpeg'
                ],
                [
                    'album_id' => 'BF1015002',
                    'format' => '10x15',
                    'name' => 'A2-100',
                    'price' =>'3.00',
                    'capacity' => '100',
                    'photo_link' => 'http://fotojetoni.com/albums/A2-100.jpeg'
                ],
                [
                    'album_id' => 'BF1015003',
                    'format' => '10x15',
                    'name' => 'A3-100',
                    'price' =>'3.00',
                    'capacity' => '100',
                    'photo_link' => 'http://fotojetoni.com/albums/A3-100.jpeg'
                ],
                [
                    'album_id' => 'BF1015004',
                    'format' => '10x15',
                    'name' => 'A4-100',
                    'price' =>'3.00',
                    'capacity' => '100',
                    'photo_link' => 'http://fotojetoni.com/albums/A4-100.jpeg'
                ],
                        [
                            'album_id' => 'BF1015005',
                            'format' => '10x15',
                            'name' => 'A5-100',
                            'price' =>'3.00',
                            'capacity' => '100',
                            'photo_link' => 'http://fotojetoni.com/albums/A5-100.jpeg'
                        ]
            ]
        );

        return response() -> json($config);

    }
}
