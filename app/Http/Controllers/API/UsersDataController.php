<?php
/**
 * Created by PhpStorm.
 * User: valto
 * Date: 5/4/2018
 * Time: 8:27 PM
 */

namespace App\Http\Controllers\API;

use App\UserData;
use App\UserProduct;
use App\User;
use App\NotRegisteredUserData;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;

class UsersDataController extends Controller
{
    public $successStatus = 200;

//    Used to upload images(mobile)
    public function product(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'platform' => 'nullable',
            'image_files_url' => 'required',
            'image_number' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        if ($request->hasFile('image_files_url')) {
            $userId = $request->input('user_id');
            $zipName = User::where('id', $userId)->value('name');
            $fileName = $zipName . '-' . $userId . '-' . time() . '.zip';

            $file = $request->file('image_files_url');
            try {
                $file->storeAs('public/uploads', $fileName);
                $respone = true;
            } catch (\Exception $e) {
                $respone = false;
            }
        }
        $userProduct = new UserProduct();
        $userProduct->user_id = $request->input('user_id');
        $userProduct->platform = $request->input('platform');
        $userProduct->image_number = $request->input('image_number');
        $userProduct->image_files_url = $fileName;

        try {
            $userProduct->save();
            $respone = true;
        } catch (\Exception $e) {
            $respone = false;
        } finally {
            return response()->json(['success' => $respone], $this->successStatus);
        }
    }

//    Used to upload images(Web)
    public function upload(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'image_files_url' => 'required',
            'image_number' => 'required'
        ]);
        if ($validator->fails()) {

            return response()->json(['error' => $validator->errors()], 401);
        }

        if ($request->hasFile('image_files_url')) {


            $userId = $request->input('user_id');
            $zipName = User::where('id', $userId)->value('name');
            $zipper = new \Chumper\Zipper\Zipper;
            $fileName = $zipName . '-' . $userId . '-' . time() . '.zip';
            try {
                $index = 0;
                $zipper->make('public/uploads/' . $fileName);
                foreach ($request->file('image_files_url') as $img) {
                    $index++;
                    $fileputname = $img->getClientOriginalName();
                    $fileToStore = $index . '_' . $fileputname;
                    $zipper->add($img, $fileToStore);
                }
            } catch (\Exception $e) {
                $respone = false;
            }
            $zipper->close();

        }

        $userProduct = new UserProduct();
        $userProduct->user_id = $request->input('user_id');
        $userProduct->image_number = $request->input('image_number');
        $userProduct->image_files_url = $fileName;

        try {
            $userProduct->save();
            $respone = true;
        } catch (\Exception $e) {
            $respone = false;
        } finally {
            return response()->json(['success' => $respone], $this->successStatus);
        }
    }


//    Used to register and upload data for not registered users
    public function not_registered_user_data(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'other_name' => 'required',
            'other_lastName' => 'required',
            'other_phone' => 'required',
            'other_city' => 'required',
            'other_email' => 'nullable',
            'other_address' => 'required',
            'other_terms_conditions' => 'required',
            'platform' => 'nullable',
            'other_image_files_url' => 'required',
            'other_image_number' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        if ($request->hasFile('other_image_files_url')) {

            $name = $request->input('other_name');

            $fileName = $name . '-' . time() . '.zip';

            $file = $request->file('other_image_files_url');

            try {
                $file->storeAs('public/uploads', $fileName);
                $respone = true;
            } catch (\Exception $e) {
                $respone = false;
            }
        }
        $userNotLoggedInData = new NotRegisteredUserData();
        $userNotLoggedInData->other_name = $request->input('other_name');
        $userNotLoggedInData->other_lastName = $request->input('other_lastName');
        $userNotLoggedInData->other_phone = $request->input('other_phone');
        $userNotLoggedInData->other_email = $request->input('other_email');
        $userNotLoggedInData->other_city = $request->input('other_city');
        $userNotLoggedInData->other_address = $request->input('other_address');
        $userNotLoggedInData->other_terms_conditions = $request->input('other_terms_conditions');
        $userNotLoggedInData->platform = $request->input('platform');
        $userNotLoggedInData->other_image_number = $request->input('other_image_number');
        $userNotLoggedInData->other_image_files_url = $fileName;

        try {
            $userNotLoggedInData->save();
            $respone = true;
        } catch (\Exception $e) {
            $respone = false;
        } finally {
            return response()->json(['success' => $respone], $this->successStatus);
        }


    }


//    used just for web
    public function upload_not_registered(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'other_name' => 'required',
            'other_lastName' => 'required',
            'other_phone' => 'required',
            'other_city' => 'required',
            'other_email' => 'nullable',
            'other_address' => 'required',
            'other_terms_conditions' => 'required',
            'other_image_files_url' => 'required',
            'other_image_number' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        if ($request->hasFile('other_image_files_url')) {

            $zipName = $request->input('other_name');
            $zipper = new \Chumper\Zipper\Zipper;
            $fileName = $zipName . '-' . time() . '.zip';

            try {
                $zipper->make('storage/uploads/' . $fileName);
                $index = 0;
                foreach ($request->file('other_image_files_url') as $img) {
                    $index++;
                    $fileputname = $img->getClientOriginalName();
                    $fileToStore = $index . '_' . $fileputname;
                    $zipper->add($img, $fileToStore);
                }
                $zipper->close();
            } catch (\Exception $e) {
                $respone = false;
            }

        }

        $userNotLoggedInData = new NotRegisteredUserData();
        $userNotLoggedInData->other_name = $request->input('other_name');
        $userNotLoggedInData->other_lastName = $request->input('other_lastName');
        $userNotLoggedInData->other_phone = $request->input('other_phone');
        $userNotLoggedInData->other_email = $request->input('other_email');
        $userNotLoggedInData->other_city = $request->input('other_city');
        $userNotLoggedInData->other_address = $request->input('other_address');
        $userNotLoggedInData->other_terms_conditions = $request->input('other_terms_conditions');
        $userNotLoggedInData->other_image_number = $request->input('other_image_number');
        $userNotLoggedInData->other_image_files_url = $fileName;

        try {
            $userNotLoggedInData->save();
            $respone = true;
        } catch (\Exception $e) {
            $respone = false;
        } finally {
            return response()->json(['success' => $respone], $this->successStatus);
        }
    }


    /**
     * Returns data of the user
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function userData($id)
    {
        $user = User::find($id);

        return response()->json([
            'email' => $user->email,
            'name' => $user->name,
            'lastName' => $user->lastName,
            'phone' => $user->phone,
            'city' => $user->email,
            'address' => $user->address,
        ]);
    }

    /**
     * Function to edit data with PUT
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function editData(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $validator = Validator::make($request->all(), [
            'email' =>[ 'required', Rule::unique('users')->ignore($user->id)],
            'name' => 'required',
            'lastName' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }

        $user->update($request->all());

        return response()->json(['success' => true], $this->successStatus);
    }


}
