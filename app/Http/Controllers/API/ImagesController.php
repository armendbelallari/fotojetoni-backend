<?php
/**
 * Created by PhpStorm.
 * User: valto
 * Date: 5/4/2018
 * Time: 8:27 PM
 */

namespace App\Http\Controllers\API;

use App\UserData;
use App\UserProduct;
use App\User;
use App\NotRegisteredUserData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class ImagesController extends Controller
{
    public $successStatus = 200;

    public function store(Request $request) {
        $receiver = new FileReceiver('images', $request, HandlerFactory::classFromRequest($request));

        if ($receiver->isUploaded() === false) {
            throw new UploadMissingFileException();
        }

        $save = $receiver->receive();

        if ($save->isFinished()) {
            $file = $save->getFile();

            $filename = str_slug(basename(time(), '.'.$file->getClientOriginalExtension())).'.'.$file->getClientOriginalExtension();;
            $mime = str_replace('/', '-', $file->getMimeType());

            $file->move(storage_path().'/app/media/images/', $filename);

            return response()->json([
                'path' => storage_path().'/app/media/images/'.$filename,
                'name' => $filename,
                'mime_type' => $mime,
            ]);
        }

        $handler = $save->handler();
        return response()->json([
            'done' => $handler->getPercentageDone()
        ]);
    }


}
