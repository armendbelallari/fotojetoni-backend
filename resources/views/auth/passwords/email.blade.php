@extends('layouts.app')

@section('content')

    <div class="bodyDiv">
        <div class="emailPlace">
            <div class="headerPlace">
                <h5>Nderro fjalkalimin</h5>
                <hr>
            </div>

            <div>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="mt-3" method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Email :</label>

                        <div class="col-md-6">
                            <input id="email" type="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                        <strong>Ky email nuk egziston</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-6 mt-3 offset-md-4">
                            <button type="submit" class="btn orangeBtn">
                                Dergo linkun e resetimit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
