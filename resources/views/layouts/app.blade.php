<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title></title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body{
            background-color: #eda80c;

        }
        .bodyDiv{
            height: 100vh;
        }
        .emailPlace {
            width: 600px;
            height: 250px;
            background-color: #fff;
            border-radius: 30px;
            position: relative;
            top: 45%;
            margin: 0 auto;
            transform: translateY(-50%);
        }
        .passwordPlace {
            width: 600px;
            height: 350px;
            background-color: #fff;
            border-radius: 30px;
            position: relative;
            top: 50%;
            margin: 0 auto;
            transform: translateY(-50%);
        }
        .headerPlace{
            width: 100%;
            padding: 20px 45px;
            text-align: center;
        }
        hr{
            margin: 0;
            height: 2px;
            box-shadow: none !important;
            background-color: #eda80c;
        }
        h5{
            font-weight: bold;
            margin-bottom: 18px;
        }
        .orangeBtn{
            background-color: rgb(252,88,7);
            border: 2px solid rgb(252,88,7);
            color: #fff;
        }
        .orangeBtn:active{

            background-color: #d54c00;
            border: 2px solid #d54c00;
            color: #fff;
        }
        input{
            outline: none !important;
            box-shadow: none !important;
        }
        input:focus{
            border-color: rgb(252,88,7) !important;
        }
    </style>
</head>
<body>
    <div id="app">
        <main>
            @yield('content')
        </main>
    </div>
</body>
</html>
