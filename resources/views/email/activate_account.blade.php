
<html>
<head>
    <title>Konfirmimi</title>
    <style>
        body {
            width: 100%;
            height: 100vh;
            margin: 0;
            padding: 0;
            background-color: #eda80c;
            text-align: center;
        }

        h3{
            margin: 0;
            margin-bottom: 5px;
        }
        p {
            margin: 0;
            margin-bottom: 5px;
        }

    </style>
</head>
<body>

<div class="confirmationBody">
    <h3>Kliko linkun per te konfirmuar email-in</h3>
    <p>
        Kliko linkun e bashkangjitur per te konfirmuar email-in dhe per ta aktivizuar llogaritne {{ url('/api/activate/account/' . $email_token) }}
    </p>
</div>

</body>
</html>


