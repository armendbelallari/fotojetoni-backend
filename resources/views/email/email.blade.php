<html>
<head>
    <title>Konfirmimi</title>
    <style>
        body {
            width: 100%;
            height: 100vh;
            margin: 0;
            padding: 0;
            background-color: #eda80c;
            text-align: center;
        }

        .confirmationBody {
            box-shadow: 0px 5px 20px 0px rgba(237, 102, 0, 0.74);
            max-width: 500px;
            height: 500px;
            border-radius: 30px;
            background-color: #fff;
            position: relative;
            padding: 25px;
            top: 50%;
            margin: 0 auto;
            transform: translateY(-50%);
            font-size: 20px;
        }
        h3{
            margin: 0;
            margin-bottom: 5px;
        }
        p {
            margin: 0;
            margin-bottom: 5px;
        }

        a {
            color: #eda80c;
        }

    </style>
</head>
<body>

<div class="confirmationBody">
    <h3>Kliko linkun per te konfirmuar email-in</h3>
    <p>
        Kliko linkun e bashkangjitur per te konfirmuar email-in dhe per ta aktivizuar llogaritne {{ url('/activate/account/' . $email_token) }}
    </p>
</div>

</body>
</html>


