<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#ff7332">
    <meta name="msapplication-navbutton-color" content="#ff7332">
    <meta name="apple-mobile-web-app-status-bar-style" content="#ff7332">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178246991-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-178246991-1');
    </script>
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '910558222768170');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=910558222768170&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->

  <title>BonFoto App</title>

  <!-- Bootstrap core CSS -->
  <link href="{{url('frontend/al/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

</head>

<body>
    
    <style>
        .fotot_home {width:100%;}
        body {background: #000 !important; color: #fff !important;}
        .social {max-width: 64px; margin-bottom: 20px;}
    </style>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top" style="background: #ff7332 !important;">
    <div class="container">
      <a class="navbar-brand" href="/">BonFoto App</a>
      <a class="nav-link" style="color: #fff" href="/informacione">Informacione</a>
    </div>  
  </nav>

  <!-- Page Content -->
  <div class="container" id="home">
    <div class="row">
      <div class="col-lg-12 text-center" style="margin-bottom: 1rem!important;">
        <h1 class="mt-5">4 vjet jetë BonFoto</h1>
          
          <div class="row">
            <div class="col-lg-6 mt-5 offset-lg-3">

            <p>Informacione bazike mbi ofertat dhe shpërblimet e BonFoto për 4 vjetorin e themelimit.</p>

            <p>4 vjetorin e festojmë me 3 super dhurata dhe 3 super shpërblime. BonFoto dhe bëhu pjesë e lojës shpërblyese.</p>

            <p>100 foto + 4 foto A5 + transporti + 1 kupon për lojë shpërblyese = 14.90€;</p>

            <p>300 foto + 4 foto A5 + transporti + 5 kupona për lojë shpërblyese = 39.90€;</p>

            <p>500 foto + 4 foto A5 + Transporti + 10 kupona për lojë shpërblyese = 59.90€.</p>

            <p>Kuponat vlejnë për lojën shpërblyese, fituesit do të jenë 3:</p>

            <p>•Spinner360Video për një ditë pa pagesë;</p>
            <p>•2 netë në Hotel Sharri në Prevallë për çift (fjetja, mengjesi dhe SPA)</p>
            <p>•2 netë në Grand Blue Fafa në Durrës për çift (fjetja, mëngjesi dhe SPA).</p>

            <p>Sa më shumë kupona, aq më të mëdha gjasat për të fituar.</p>
            <p>Oferta zgjatë një javë (deri më 18.09.2022), ndërsa tërheqja e kuponave fitues bëhet LIVE me datë 23.09.2022 në ora 20.00</p>

                <!-- <p>
                100 foto + album + transporti = 13.50&euro;
                </p>

                <p>
                    300 foto + transporti = 33.90&euro;
                </p>

                <p>
                    Mbi 500 foto= 10 cent 1 foto (transporti gratis)
                </p>

                <p>
                    Per Shqiperi, oferta e njejte kushton 1 euro me shume.
                </p>

                <p>
                    Fotografite jane me dimensione 10x15 cm.
                    Porosite mund te vonohen pak me shume se diteve te zakonshme.
                </p>

                <p>
                    Fotografite duhet t’i dergoni permes aplikacionit BonFoto.
                </p>

                <p>
                    Oferta vlen deri me 05.12.2021
                </p> -->

                <p class="mt-3 mb-5">
                    Per informacione shtese, na kontaktoni ne Instagram, Facebook apo ne tel: +38349666346
                </p>
            </div>
          </div>
         
          <br /><br />
        <p class="lead">BonFoto &euml;sht&euml; aplikacion q&euml; sh&euml;rben p&euml;r d&euml;rgimin e fotografive p&euml;r printim, nga p&euml;rdoruesi tek kompania Foto Jetoni. <br />I leht&euml; p&euml;r p&euml;rdorim, modern dhe i p&euml;rshtatsh&euml;m.</p>
          <a href="https://apps.apple.com/us/app/bonfoto/id1412344071"><img src="{{url('frontend/al/img/bonfoto-ios.png')}}" style="max-height: 50px; background: white; padding: 2px;" /> </a><a href="https://play.google.com/store/apps/details?id=com.fotojetoni.bonfoto"><img src="frontend/al/img/bonfoto-android.png" style="max-height: 50px; background: white; padding: 2px;" /></a>
          <br /><br />
          <a href="https://www.facebook.com/BonFotoApp">
                <img src="{{url('frontend/al/img/fb_icon.png')}}" class="social" />
            </a>
          <a href="https://instagram.com/bonfoto.app">
            <img src="{{url('frontend/al/img/ig_icon.png')}}" class="social" />  
            </a>
          
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script src="{{url('frontend/al/vendor/jquery/jquery.slim.min.js')}}"></script>
  <script src="{{url('frontend/al/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>