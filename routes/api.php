<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::get('config', 'API\ConfigDataController@config');

Route::get('activate/account/{token}', 'API\UserController@activate');

Route::post('user_data', 'API\UsersDataController@user_data');

//Returns config datas
Route::get('config', 'API\ConfigDataController@config');

//useless
Route::post('upload', 'API\UsersDataController@upload');
Route::post('upload_not_registered', 'API\UsersDataController@upload_not_registered');

//Upload files
Route::post('product', 'API\UsersDataController@product');
Route::post('not_registered_user_data', 'API\UsersDataController@not_registered_user_data');

//returns clients with upload files
Route::get('new_client_request', 'API\AdminPanelController@new_client_request');
Route::get('new_notregistered_client_request', 'API\AdminPanelController@new_notregistered_client_request');

// Makes status('isdone'=1)
Route::get('file/{filename}', 'API\AdminPanelController@getFile');
Route::get('filenotreg/{filename}', 'API\AdminPanelController@getFile_notregistered');

//Returns all downloaded Clients
Route::get('downloadedclient', 'API\AdminPanelController@downloadedClient');
Route::get('downloadedclientnr', 'API\AdminPanelController@downloadedClientNR');

//Delete file
Route::get('deleteClientFile/{filename}', 'API\AdminPanelController@deleteClientFile');
Route::get('deleteNonClientFile/{filename}', 'API\AdminPanelController@deleteNonClientFile');

Route::put('editData/{id}', 'API\UsersDataController@editData');
Route::get('userData/{id}', 'API\UsersDataController@userData');

Route::post('done_notregistered_client', 'API\AdminPanelController@done_notregistered_client');
