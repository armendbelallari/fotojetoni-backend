<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('lastName');
            $table->string('phone');
            $table->string('city');
            $table->string('country')->nullable();
            $table->string('address');
            $table->string('street')->nullable();
            $table->boolean('termsCondition');
            $table->tinyInteger('verified')->default(0);
            $table->string('email_token')->nullable();

            //added by koklle
            $table->string('activation_code')->nullable();
            $table->boolean('status')->default(0);
            //end

            $table->rememberToken();
            $table->timestamps();
        });
    }//sdi sa po mkupton, ky ska ku e mer id per mometin po po
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
