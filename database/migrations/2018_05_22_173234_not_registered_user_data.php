<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NotRegisteredUserData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('not_registered_user_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('other_name');
            $table->string('other_lastName');
            $table->string('other_phone');
            $table->string('other_email');
            $table->string('other_city');
            $table->string('other_address');
            $table->boolean('other_terms_conditions');
            $table->string('other_image_files_url');
            $table->integer('other_image_number');
            $table->boolean('other_is_done')->nullable();
            $table->dateTime('other_done_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('not_registered_user_data');

    }
}
